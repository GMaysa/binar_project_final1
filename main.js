    const navbar = document.querySelector('#navbar')
    const wrap = document.querySelector('#nav__wrap')

    function hideBar() {
        navbar.classList.remove('mobile')
        navbar.classList.add('max-sm:hidden')
        wrap.classList.remove('wrapper')
    }

    function showBar() {
        wrap.classList.add('wrapper')
        navbar.classList.add('mobile')
        navbar.classList.remove('max-sm:hidden')
    }

    // END OF THE SIDEBAR SECTION

    window.addEventListener('scroll', () => {
        document.querySelector('nav').classList.toggle('navbar__scrolled', window.scrollY > 0)
    })

// ========== END OF NAVBAR SECTION ==========

    const quests = document.querySelectorAll('.faq')

    quests.forEach(quest => {
        quest.addEventListener('click', () => {
            quest.classList.toggle('show');

            const icon = quest.querySelector('.question i')
            if(icon.className === 'uil uil-angle-down text-[24px]'){
                icon.className = 'uil uil-angle-up text-[24px]'
            } else {
                icon.className = 'uil uil-angle-down text-[24px]'
            }
        })
    })